package com.mingkwan.arduinoserialcommunication;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Context;
import android.hardware.usb.UsbDevice;
import android.hardware.usb.UsbManager;
import android.os.Bundle;
import android.view.View;

import com.google.android.material.snackbar.Snackbar;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class SelectDeviceActivity extends AppCompatActivity {

    UsbManager manager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_select_device);

        manager = (UsbManager) getSystemService(Context.USB_SERVICE);
        // get list of attached USB devices to show to user
        HashMap<String, UsbDevice> usbDevices = manager.getDeviceList();
        List<Object> deviceList = new ArrayList<>();
        if (!usbDevices.isEmpty()) {
            // get name and VID of each device
            for (Map.Entry<String, UsbDevice> entry : usbDevices.entrySet()) {
                UsbDevice device = entry.getValue();
                String deviceName = device.getProductName();
                int deviceVID = device.getVendorId();
                DeviceInfoModel deviceInfoModel = new DeviceInfoModel(deviceName, deviceVID);
                deviceList.add(deviceInfoModel);
            }

            // display devices
            RecyclerView recyclerView = findViewById(R.id.recyclerViewDevice);
            recyclerView.setLayoutManager(new LinearLayoutManager(this));
            DeviceListAdapter deviceListAdapter = new DeviceListAdapter(this, deviceList);
            recyclerView.setAdapter(deviceListAdapter);
            recyclerView.setItemAnimator(new DefaultItemAnimator());
        } else {
            View view = findViewById(R.id.recyclerViewDevice);
            Snackbar snackbar = Snackbar.make(view, getString(R.string.no_attached_devices_toast), Snackbar.LENGTH_LONG);
            snackbar.show();
        }
    }
}