package com.mingkwan.arduinoserialcommunication;

import android.app.Activity;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.hardware.usb.UsbDevice;
import android.hardware.usb.UsbDeviceConnection;
import android.hardware.usb.UsbManager;
import android.util.Log;

import com.felhr.usbserial.UsbSerialDevice;
import com.felhr.usbserial.UsbSerialInterface;

import java.io.UnsupportedEncodingException;
import java.util.HashMap;
import java.util.Map;

public class USBCommunicator {
    // https://blog.fossasia.org/neurolab-data-transfer-establishing-serial-communication-between-arduino-and-android/
    private static final String ACTION_USB_PERMISSION = "com.android.example.USB_PERMISSION";
    private static final String TAG = "USBCommunicator";

    private int deviceVID;

    private UsbManager manager;
    private UsbDevice device;
    private UsbDeviceConnection connection;
    private UsbSerialDevice serialPort;
    private MainActivity main;

    private UsbSerialInterface.UsbReadCallback mCallback = new UsbSerialInterface.UsbReadCallback() {
        @Override
        public void onReceivedData(byte[] data) {
            try {
                String dataString = new String(data, "UTF-8").replaceAll("[^\\d.]", "");
                Log.d(TAG, "Brightness: " + dataString + " lux");

                int lux = Integer.parseInt(dataString);
                main.runOnUiThread(new Runnable() {

                    @Override
                    public void run() {
                        //main.appendText("Brightness " + lux + " lux");
                        main.setBrightnessFromLux(lux);
                    }
                });

            } catch (UnsupportedEncodingException e) {
                e.printStackTrace();
            } catch (NumberFormatException e){

            }
        }
    };

    public USBCommunicator (UsbManager manager, MainActivity main, int deviceVID) {
        this.main = main;
        this.manager = manager;
        this.deviceVID = deviceVID;
    }

    public void searchForArduinoDevice (Context context) {
        HashMap<String, UsbDevice> usbDevices = manager.getDeviceList();

        boolean keep = true;
        String tViewText = "";
        String buttonText = main.getString(R.string.select_button_text);
        if (!usbDevices.isEmpty()) {
            for (Map.Entry<String, UsbDevice> entry : usbDevices.entrySet()) {
                device = entry.getValue();

                int deviceVID = device.getVendorId();
                if (deviceVID == this.deviceVID) {
                    Log.d(TAG, "Found Arduino, requesting Permission");
                    PendingIntent pi = PendingIntent.getBroadcast(context, 0, new Intent(ACTION_USB_PERMISSION), 0);
                    manager.requestPermission(device, pi);
                    keep = false;
                    tViewText = main.getString(R.string.device_connected_text);
                    buttonText = main.getString(R.string.disconnect_button_text);
                } else {
                    device = null;
                }

                if (!keep)
                    break;
            }
        } else {
            tViewText = main.getString(R.string.no_device_attached_text);
            this.deviceVID = -1;
        }

        if(keep) {
            stopConnection();
            tViewText = main.getString(R.string.no_device_found_text);
            this.deviceVID = -1;
        }

        String finalTViewText = tViewText;
        String finalButtonText = buttonText;
        int finalDeviceVID = this.deviceVID;
        boolean finalKeep = keep;
        main.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                main.appendText(finalTViewText);
                main.setButtonText(finalButtonText);
                main.setNewVID(finalDeviceVID);
                if (finalKeep)
                    main.setConnected(true);
                else
                    main.setConnected(false);
            }
        });
    }

    private final BroadcastReceiver broadcastReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            if (intent.getAction().equals(ACTION_USB_PERMISSION)) {
                boolean granted = intent.getExtras().getBoolean(UsbManager.EXTRA_PERMISSION_GRANTED);
                if (granted) {
                    connection = manager.openDevice(device);
                    serialPort = UsbSerialDevice.createUsbSerialDevice(device, connection);
                    if (serialPort != null) {
                        if (serialPort.open()) {
                            serialPort.setBaudRate(9600);
                            serialPort.setDataBits(UsbSerialInterface.DATA_BITS_8);
                            serialPort.setStopBits(UsbSerialInterface.STOP_BITS_1);
                            serialPort.setParity(UsbSerialInterface.PARITY_NONE);
                            serialPort.setFlowControl(UsbSerialInterface.FLOW_CONTROL_OFF);
                            serialPort.read(mCallback);
                            Log.d(TAG, "Serial Connection Opened!");
                        } else {
                            Log.d(TAG, "PORT NOT OPEN");
                        }
                    } else {
                        Log.d(TAG, "PORT IS NULL");
                    }
                } else {
                    Log.d(TAG, "PERM NOT GRANTED");
                }
            } else if (intent.getAction().equals(UsbManager.ACTION_USB_DEVICE_ATTACHED)) {
                searchForArduinoDevice(context);
            } else if (intent.getAction().equals(UsbManager.ACTION_USB_DEVICE_DETACHED)) {
                stopConnection();
            }
        }
    };

    public void stopConnection() {
        if (serialPort != null)
            serialPort.close();
        main.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                main.appendText(main.getString(R.string.device_disconnected_text));
                main.setButtonText(main.getString(R.string.connect_button_text));
            }
        });
        Log.d(TAG, "Serial connection closed");
    }

    public BroadcastReceiver getBroadcastReceiver() {
        return broadcastReceiver;
    }

    public void setVID(int vid) {
        this.deviceVID = vid;
    }


}
