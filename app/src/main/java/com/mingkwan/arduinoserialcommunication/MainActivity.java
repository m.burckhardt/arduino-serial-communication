package com.mingkwan.arduinoserialcommunication;

import androidx.appcompat.app.AppCompatActivity;

import android.content.ContentResolver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.hardware.usb.UsbManager;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.provider.Settings;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import static android.preference.PreferenceManager.getDefaultSharedPreferences;

public class MainActivity extends AppCompatActivity {

    // just to filter intent
    private static final String ACTION_USB_PERMISSION = "com.android.example.USB_PERMISSION";
    private static final String TAG = "USBCommunicator";

    private TextView tView;
    private Button buttonConnect;

    private UsbManager manager;
    private USBCommunicator communicator;
    private ContentResolver cResolver;

    private SharedPreferences sharedPreferences;
    private int deviceVID;
    private String deviceName;
    private boolean hasPermissions;
    private boolean isConnected;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        tView = findViewById(R.id.textView);
        buttonConnect = findViewById(R.id.button);

        // check if VID was set previously
        sharedPreferences = getDefaultSharedPreferences(this);
        deviceVID = sharedPreferences.getInt(getString(R.string.device_vid_key), -1);

        // initialize USB communicator
        manager = (UsbManager) getSystemService(Context.USB_SERVICE);
        communicator = new USBCommunicator(manager, this, deviceVID);
        cResolver = getContentResolver();

        IntentFilter filter = new IntentFilter();
        filter.addAction(ACTION_USB_PERMISSION);
        filter.addAction(UsbManager.ACTION_USB_DEVICE_ATTACHED);
        filter.addAction(UsbManager.ACTION_USB_DEVICE_DETACHED);
        registerReceiver(communicator.getBroadcastReceiver(), filter);

        // if a device has been selected from SelectDeviceActivity
        deviceName = getIntent().getStringExtra("deviceName");
        if (deviceName != null) {
            // set new VID
            setNewVID(getIntent().getIntExtra("deviceVID", -1));
        }

        isConnected = false;
        hasPermissions = handlePermissions();
        if(hasPermissions) {
            // allowed to write to system settings -> change brightness mode to manual
            Settings.System.putInt(cResolver, Settings.System.SCREEN_BRIGHTNESS_MODE, Settings.System.SCREEN_BRIGHTNESS_MODE_MANUAL);

            // set text to show Arduino deviceID
            if (deviceVID > 0) {
                // device ID was set -> show connect text/button
                tView.setText(getString(R.string.selected_text, deviceVID));
                buttonConnect.setText(getString(R.string.connect_button_text));
            }
            else {
                // device ID not set -> show select text/button
                tView.setText(getString(R.string.no_selected_text));
                buttonConnect.setText(getString(R.string.select_button_text));
            }
        } else {
            // not allowed to write to settings -> show info
            tView.setText(getString(R.string.no_permissions_text));
            buttonConnect.setText(getString(R.string.no_permissions_text_button));
        }

    }

    @Override
    protected void onDestroy() {
        // restore automatic brightness mode
        Settings.System.putInt(cResolver, Settings.System.SCREEN_BRIGHTNESS_MODE, Settings.System.SCREEN_BRIGHTNESS_MODE_AUTOMATIC);
        super.onDestroy();
    }

    void appendText(String text) {
        this.tView.append("\n" + text);
    }

    void setButtonText(String text) {
        this.buttonConnect.setText(text);
    }

    public void onConnectButton(View view) {
        if (!this.hasPermissions)
            // request permissions if not yet granted
            handlePermissions();
        if (isConnected)
            this.communicator.stopConnection();
        else if (this.deviceVID > 0)
            // start connection if VID selected
            this.communicator.searchForArduinoDevice(this);
        else {
            // choose VID if not yet selected
            Intent intent = new Intent(MainActivity.this, SelectDeviceActivity.class);
            startActivity(intent);
        }
    }

    void setNewVID(int id) {
        if (id == this.deviceVID)
            return;

        this.deviceVID = id;

        // update data for next launch
        SharedPreferences.Editor editor = this.sharedPreferences.edit();
        editor.putInt(getString(R.string.device_vid_key), this.deviceVID);
        editor.apply();

        // start connection
        this.communicator.setVID(this.deviceVID);
        this.communicator.searchForArduinoDevice(this);
    }

    private boolean handlePermissions() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M && !Settings.System.canWrite(this)) {
            Intent intent = new Intent(Settings.ACTION_MANAGE_WRITE_SETTINGS);
            intent.setData(Uri.parse("package:" + this.getPackageName()));
            startActivity(intent);
        }

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M)
            return Settings.System.canWrite(this);
        else
            return true;
    }

    private void changeScreenBrightness(int brightness) {
        if (brightness < 1)
            brightness = 1;
        if (brightness > 255)
            brightness = 255;

        Settings.System.putInt(cResolver, Settings.System.SCREEN_BRIGHTNESS, brightness);
    }

    void setBrightnessFromLux(int lux) {
        // formula from https://www.maximintegrated.com/en/design/technical-documents/app-notes/4/4913.html
        // lux to percent
        double brightPercent = Math.min(100, (9.323 * Math.log(Math.max(lux, 1)) + 27.059));

        int brightness = (int) (brightPercent * 2.55);

        //appendText("Setting brightness to " + (int) brightPercent + "% or cResolver value " + brightness);
        changeScreenBrightness(brightness);
    }

    void setConnected(boolean connected) {
        isConnected = connected;
    }
}