package com.mingkwan.arduinoserialcommunication;

public class DeviceInfoModel {

    private String deviceName;
    private int deviceVID;

    public DeviceInfoModel() {}

    public DeviceInfoModel(String deviceName, int deviceVID) {
        this.deviceName = deviceName;
        this.deviceVID = deviceVID;
    }

    public String getDeviceName() {
        return deviceName;
    }

    public int getDeviceVID() {
        return deviceVID;
    }
}
